(function () {
  "use strict";
  var form = document.forms[0],
      items = {},
      aID = location.search.split("=")[1];
  items[aID] = true;
  chrome.storage.local.set(items);
  form.rating.value = "9999";
  form.other_rating.value = "1013";
  form.submit();
}).call(this);

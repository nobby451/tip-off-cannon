(function () {
  "use strict";
  Array.apply(null, document.querySelectorAll(".a1wrp > h3 > a, .cf > h3 > a")).forEach(function (element) {
    var aID = element.pathname.split("/")[3],
        origin = element.origin;
    chrome.storage.local.get(aID, function (items) {
      var tipOff = document.createElement("button"),
          toggle = document.createElement("button"),
          excluded = items[aID];
      tipOff.disabled = excluded;
      tipOff.textContent = "通報";
      tipOff.type = "button";
      tipOff.classList.add("auction__button-tip-off");
      tipOff.addEventListener("click", function () {
        var formData = new FormData(),
            xhr = new XMLHttpRequest();
        formData.append("aID", aID);
        formData.append("rating", 9999);
        formData.append("other_rating", 1013);
        xhr.responseType = "document";
        xhr.addEventListener("load", function () {
          var items = {};
          items[aID] = tipOff.disabled = true;
          chrome.storage.local.set(items);
        });
        xhr.open("POST", origin.replace("page", "navi") + "/jp/config/review");
        xhr.send(formData);
      });
      toggle.textContent = excluded ? "復活" : "除外";
      toggle.type = "button";
      toggle.classList.add("auction__button-toggle");
      toggle.addEventListener("click", function () {
        items[aID] = !excluded;
        chrome.storage.local.set(items);
      });
      element.parentElement.parentElement.appendChild(tipOff);
      element.parentElement.parentElement.appendChild(toggle);
    });
  });
}).call(this);

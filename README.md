# 転売通報補助ツール

ヤフオクの転売通報を支援するChrome(Chromium)拡張機能です。

ちゃんとしたのを作るまでの繋ぎなので、今のところ検索結果から1件ずつワンクリックで自動通報する程度の機能です。

元々5th LoveLive!のチケット転売を潰すために作りましたが、中身を少しいじればヤフオクのどんな通報にも対応できると思います。

## インストール

1. 左のメニューからダウンロードを選択
2. Download repositoryをクリックしてツールをダウンロード
3. ダウンロードしたzipを適当な場所に解凍
4. Chromeの右上のメニューから、ツール→拡張機能を選択
5. 先ほど解凍して出てきたフォルダーを、Chromeの拡張機能のページにドラッグアンドドロップ

## 使い方

1. ChromeでYahooにログイン
2. ヤフオクで潰したいオークションを検索
3. 検索結果リスト1件1件の見出し下部に『通報』というボタンが自動生成される
4. 潰したいオークションの『通報』ボタンをクリックで通報完了

※既知の不具合として、ボタンが生成される前に見出しをクリックすると、そのオークションの通報ボタンが機能しなくなります。

※未ログイン状態で通報ボタンを押しても、通報失敗した上で通報済みになってしまうので、ログイン済みであることをよく確認してください。

## アンインストール

1. Chromeの右上のメニューから、ツール→拡張機能を選択
2. yahoo auctions tip off cannonの右の方にあるゴミ箱アイコンをクリック

※一時的に無効にしたい場合は有効のチェックを外すだけでも構いません。
